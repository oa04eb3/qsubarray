#!/bin/bash



[ -z "$1" ] &&  echo "usage: $0 [-f] [ -t tag ]<install_dir> [ <modulesfiles_dir> ] " && echo "$0 ../appli/qsubarray ../appli/modulefiles" && exit 1

force="false"
[ "$1" == "-f" ] && force="true" && shift

tag="dev"
[ "$1" == "-t" ] && tag="$2" && shift 2


mkdir -p $1
destdir=$(readlink -f $1)


modulefiles="$2"

script_dir=$(dirname $(readlink -f $0))

cd $script_dir

source etc/qsubarray.conf

source $QSUBARRAY_CONDA/bin/activate $QSUBARRAY_CONDA

#export PATH="$script_dir/bin:$PATH"


if [ "$force" != "true" ] ; then
    ./test_suite/test_suite.sh
    if [ $? -ne 0 ] ; then 
    	echo "Not installing, not all tests passed"
    	exit 1
    fi
fi

version=$(git log --date='short' --pretty="%cd_%h" 2>/dev/null | head -1)
[ -z "$version" ] && version=$(date "+%Y%m%d-%H%m") 

dist_dir="$destdir/dist"

version_dir="$dist_dir/$version"

echo "using $version_dir as install prefix"





# generate man pages
mkdir -p $version_dir/man/man1
mkdir -p $version_dir/man/man5
find man/ -name "*.md" | awk '{print "'$version_dir/'"$1" "$1}' | sed 's/\.md//' | xargs -t -l1 pandoc -t man -s -o 
 
#conda list -e | egrep -v '#' > etc/conda.env    
#conda_local_list=$(cat etc/conda.env )
#
#conda_dist_list=""
#conda_dist=$(readlink -f $dist_dir/share/conda/latest 2>/dev/null)
#
#conda_dist_list=$(
#	if [  -f $conda_dist/bin/activate ] ; then
#		source deactivate
#		source $conda_dist/bin/activate $conda_dist
#		conda list -e | egrep -v '#'
#	fi
#)
#
#if [ "$conda_local_list" != "$conda_dist_list" ] ; then
#	echo "conda $conda_dist out of date"
#	conda_new_name=$(date "+%Y%m%d-%H%m")
#	conda_new=$dist_dir/share/conda/$conda_new_name
#	rm -fr $conda_new 2>/dev/null
#	echo "$conda_local_list" > tmp/conda_list
#	conda create -y -p $conda_new --file tmp/conda_list
#	rm -f $dist_dir/share/conda/latest
#	(cd $dist_dir/share/conda ; ln -s $conda_new_name latest)
#	conda_dist=$(readlink -f $dist_dir/share/conda/latest 2>/dev/null)
#fi
    
echo -n "Copying files..."
cp -r bin $version_dir/
rm -f $version_dir/debug.py*
cp -r etc $version_dir/
mkdir -p $version_dir/lib
#rm -f $version_dir/lib/conda
#(cd $version_dir/lib ; ln -s $(realpath --relative-to=. $conda_dist) conda)
#set -x
#cat etc/qsubarray.conf | sed -r 's|(QSUBARRAY_CONDA)=".*"|\1="'$(realpath --relative-to=$version_dir $conda_dist)'"|'  > $version_dir/etc/qsubarray.conf
#set +x
echo "done"

rm -f $destdir/$tag 
( cd $destdir ; ln -s dist/$version $tag )

echo "symlinks :"
ls -l $destdir/$tag

#modulefiles
if [ -n "$modulefiles" ] ; then
	mkdir -p $modulefiles/qsubarray
	if [ "$tag" == "dev" ]; then
		export INSTALL_PATH=$(readlink -f .)
		echo "dev install: module load qsubarray/dev will use $INSTALL_PATH" 
	else
		export INSTALL_PATH="$destdir/$tag"
	fi
	cat modulefiles/qsubarray/versionfile | envsubst > $modulefiles/qsubarray/$tag
fi
