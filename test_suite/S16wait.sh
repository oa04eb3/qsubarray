#!/bin/bash
rm -f test_suite/tmp/S16wait_$(hostname)_workspace 2>/dev/null
echo qsubarray
seq 1 10 | qsubarray  --quiet --no-qstat --no-show-stdout --no-show-stderr --workspace-link=test_suite/tmp/S16wait_$(hostname)_workspace xargs -ISEQ bash -c 'ret=$(((SEQ % 2))) ; (( ret == 0 )) && echo SEQ ; (( ret == 1 )) && echo err SEQ >&2 ; exit 0'
echo wait stdout
qsubarray_wait --show-stdout --sort test_suite/tmp/S16wait_$(hostname)_workspace
echo wait stderr
qsubarray_wait  --show-stderr --sort test_suite/tmp/S16wait_$(hostname)_workspace 2>&1
