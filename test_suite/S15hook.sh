#!/bin/bash
rm  -f test_suite/tmp/S15hook*$(hostname)*
seq 1 2 | qsubarray "$@" -N hookfile --hook=test_suite/S15hook cat > /dev/null
[ -f test_suite/tmp/S15hook_$(hostname) ] && echo "test file ok"
seq 1 2 | qsubarray "$@" -N hookstring --hook='echo "test hook from string. QSUBARRAY_WS: $QSUBARRAY_WS" > test_suite/tmp/S15hook_$(hostname)_string' cat > /dev/null
[ -f test_suite/tmp/S15hook_$(hostname)_string ] && echo "test string ok" 
seq 1 2 | qsubarray "$@" -N wslink --workspace-link=test_suite/tmp/S15hook_$(hostname)_workspace cat > /dev/null
[ -L test_suite/tmp/S15hook_$(hostname)_workspace ] && echo "test workspace ok"
