#!/bin/bash

seq 1 10 | xargs -ISEQ bash -c "echo SEQ ; sleep 1" | qsubarray "$@" --max-lines=4 --stdin-flush-delay=3s xargs -l3 echo | xargs -n1 | sort -n | xargs

