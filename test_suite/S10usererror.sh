#!/bin/bash

rm -f ./test_suite/tmp/error_message
seq 1 10 | qsubarray --quiet --no-qstat xargs -IINPUT bash -c "echo this is an error message >&2; exit 1" >   ./test_suite/tmp/error_message 2>&1
cat ./test_suite/tmp/error_message

