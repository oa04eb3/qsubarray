#!/bin/bash

export TEST_VAR1="testvar1" ; seq 1 10 | qsubarray "$@" --sort -v TEST_VAR1 -v TEST_VAR2="test var=2",TEST_VAR3="test var 3" xargs -ISEQ bash -c 'echo "SEQ $TEST_VAR1 $TEST_VAR2 $TEST_VAR3"'
#seq 1 10 | qsubarray "$@" -v TEST_VAR2="testvar=2",TEST_VAR3="testvar3" xargs -ISEQ bash -c 'echo "SEQ $TEST_VAR1 $TEST_VAR2 $TEST_VAR3"'
