#!/bin/bash

rm -f ./test_suite/tmp/empty_stderr.$(hostname)
seq 1 10 | qsubarray --quiet --no-qstat cat | sort 2> ./test_suite/tmp/empty_stderr.$(hostname)
cat ./test_suite/tmp/empty_stderr.$(hostname)

