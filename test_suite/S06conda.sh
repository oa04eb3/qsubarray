#!/bin/bash


if [ -d /appli ] ; then
    . conda_datarmor/bin/activate ./conda_datarmor
else
    . conda/bin/activate ./conda 
fi

conda list >  ./test_suite/tmp/conda_$(hostname)_ref

echo list | qsubarray "$@" -v CONDA_PREFIX --reactivate xargs -l1 conda >  ./test_suite/tmp/conda_$(hostname)_qsub
diff -s ./test_suite/tmp/conda_$(hostname)_ref ./test_suite/tmp/conda_$(hostname)_qsub >/dev/null 2>&1 && echo "test ok"

