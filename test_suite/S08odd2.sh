#!/bin/bash

seq 1 10 | qsubarray "$@" --max-lines=100 xargs -n 2 echo | awk '{print $1}'
