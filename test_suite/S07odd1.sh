#!/bin/bash

seq 1 10 | qsubarray "$@" --max-subjobs=1 xargs -n 2 echo | awk '{print $1}'
