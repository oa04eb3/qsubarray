#!/bin/bash
rm -f test_suite/tmp/S17split_stdin_$(hostname)_workspace 2>/dev/null
seq 1 10 | qsubarray  --subjobs=3 --quiet --no-qstat --no-show-stdout --no-show-stderr --workspace-link=test_suite/tmp/S17split_stdin_$(hostname)_workspace xargs -l2 echo TEST
diff test_suite/tmp/S17split_stdin_$(hostname)_workspace/stdin test_suite/test_suite.out/S17split_stdin_ws.ref
if [ $? -eq 0 ] ; then 
    echo test ok
else
    echo test nok
fi
