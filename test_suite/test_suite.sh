#!/bin/bash

blue='\033[0;34m'
orange='\033[0;33m'
red='\033[0;31m'
green='\033[0;32m'
purple='\033[0;35m'
nocolor='\033[0m'

script_dir=$(readlink -f $(dirname $0))

export TOP_DIR=$(readlink -f $script_dir/..)

export PATH="$TOP_DIR/bin:$PATH"

pbs_version=$(qsub --version 2>&1 | sed 's/ //g' | tr "=:" "_")

all_tests=""
errors=""
success=""
unknown=""

for test_script in $script_dir/S*.sh ; do

    test_script_basename=$(basename $test_script)
    test_script_basename="${test_script_basename%.*}"

    stdout_file="$script_dir/test_suite.out/$test_script_basename.stdout.$pbs_version"
    stderr_file="$script_dir/test_suite.out/$test_script_basename.stderr.$pbs_version"
    
    rm -f $stdout_file $stderr_file

    echo "$test_script --loop-delay=5 --quiet --qstat -N $test_script_basename >$stdout_file"

done | xargs -P 5 -ICMD timeout --foreground -s INT 10m bash -c "CMD"

echo

    
for test_script in $script_dir/S*.sh ; do
    
    
    test_script_basename=$(basename $test_script)
    test_script_basename="${test_script_basename%.*}"
    
    all_tests="$all_tests $test_script_basename"
    
    printf "%-15s : " $test_script_basename
    
    stdout_file="$script_dir/test_suite.out/$test_script_basename.stdout.$pbs_version"
    stderr_file="$script_dir/test_suite.out/$test_script_basename.stderr.$pbs_version"
    
    stdout_file_ref="$script_dir/test_suite.out/$test_script_basename.ref"
    if [ -f $stdout_file_ref ] ; then
        if ! diff  $stdout_file_ref $stdout_file >/dev/null ; then
            status="${red}KO"  
            errors="$errors $test_script_basename"
        else
            status="${green}OK"
            success="$success $test_script_basename"
        fi
    else
        #cp $stdout_file $stdout_file_ref
        status="${orange}??"
        unknown="$unknown $test_script_basename"
    fi
        
    echo -e "$status$nocolor"
done

all_tests_count=$(echo $all_tests | wc -w)
errors_count=$(echo $errors | wc -w)
unknown_count=$(echo $unknown | wc -w)

if (( unknown_count != 0 )) ; then
    echo -e "${orange}$unknown_count/$all_tests_count tests unknown $nocolor"
    for unknown_ in $unknown ; do
        stdout_file="$script_dir/test_suite.out/$unknown_.stdout.$pbs_version"
        stdout_file_ref="$script_dir/test_suite.out/$unknown_.ref"
        echo "check cp $stdout_file $stdout_file_ref"
    done
    status=1
fi

if (( errors_count != 0 )) ; then
    echo -e "${red}$errors_count/$all_tests_count tests failed $nocolor"
    for error in $errors ; do
        stdout_file="$script_dir/test_suite.out/$error.stdout.$pbs_version"
        stdout_file_ref="$script_dir/test_suite.out/$error.ref"
        echo "see diff $stdout_file $stdout_file_ref"
    done
    status=1
fi

(( errors_count == 0 )) && (( unknown_count == 0 )) && echo -e "${green}All tests OK $nocolor" && status=0

exit $status





