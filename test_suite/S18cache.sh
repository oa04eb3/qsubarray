#!/bin/bash
cache_dir=test_suite/tmp/S18cache_$(hostname)
ws1=test_suite/tmp/S18cache_$(hostname)_ws1
ws2=test_suite/tmp/S18cache_$(hostname)_ws2
rm -fr $cache_dir 2>/dev/null
seq 1 10 | qsubarray  --cache=$cache_dir "$@" --workspace-link=$ws1 --max-subjobs=2 xargs -l2 echo TEST > /dev/null 
seq 1 20 | qsubarray  --cache=$cache_dir "$@" --workspace-link=$ws2 --max-subjobs=2 xargs -l2 echo TEST > /dev/null

#echo cached
#cat $ws2/stdin_lines_cached | sort
echo cache miss
cat $ws2/stdin_by_lines/stdin_lines_cache_miss.1 | sort
