#!/bin/bash
cache_dir=test_suite/tmp/S19cache_$(hostname)
ws1=test_suite/tmp/S19cache_$(hostname)_ws1
ws2=test_suite/tmp/S19cache_$(hostname)_ws2
rm -fr $cache_dir 2>/dev/null
seq 11 20| qsubarray  --sort --cache=$cache_dir "$@" --workspace-link=$ws1 --max-subjobs=2 xargs -l2 echo TEST  
seq 1 30 | qsubarray  --sort --cache=$cache_dir "$@" --workspace-link=$ws2 --max-subjobs=2 xargs -l2 echo TEST 

