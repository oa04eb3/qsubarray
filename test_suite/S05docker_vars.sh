#!/bin/bash



if which docker >/dev/null 2>&1 ; then
    DOCKER_IMAGE=ubuntu
    QSUB_OPTS="-l nodes=1:clouddocker"
else
    DOCKER_IMAGE=docker.ifremer.fr/os/ubuntu:xenial
fi

export DOCKER_IMAGE

seq 1 10 | qsubarray "$@" $QSUB_OPTS -v DOCKER_IMAGE,TEST_VAR="test var" xargs -l1 bash -c 'if egrep "docker|pbspro" /proc/1/cgroup -qa; then echo "in docker" ; echo $TEST_VAR ; else echo "not in docker" ; fi' 


