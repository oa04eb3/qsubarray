#!/bin/bash (to be sourced)


# special status for subjob and user cmd
status_queued=244
status_killed=245
status_running=246
status_bashrc_error=238

# colorize stderr if it's a terminal
blue='\033[0;34m'
orange='\033[0;33m'
red='\033[0;31m'
green='\033[0;32m'
purple='\033[0;35m'
nocolor='\033[0m'


function verbose_prefix {
    
    # return verbose prefix as a string, depending of $verbose_level
    
    local level=$1 # level asked by verbose. global is verbose_level
    
    local date=$(date "+%F %H:%M")
    local script=$(basename $0)
    local jobname=${pbs_jobname:$PBS_JOBNAME}
    local host=$(hostname)
    local call_function_index=2
    local call_function="${FUNCNAME[$call_function_index]}"
    local bash_file=$(basename ${BASH_SOURCE[$call_function_index]})
    local bt1="$bash_file:${BASH_LINENO[$call_function_index-1]}"
    
    ((call_function_index++))
    if [ $call_function_index -lt ${#FUNCNAME[@]} ] ; then
        local call_function_ancestor1="${FUNCNAME[$call_function_index]}"
        local bash_file_ancestor1=$(basename ${BASH_SOURCE[$call_function_index]})
        local bt2="(from $call_function_ancestor1 $bash_file:${BASH_LINENO[$call_function_index-1]})"
    fi
    case $verbose_level in
       
        1 ) # standard level
            printf "%s %-12s %-10s %-15s " "$date" "$host" "$jobname" "$script" ;;
        2 ) printf "%-10s %-15s " "$jobname" "$script" ;;
        3 ) printf "%-10s %-15s %-10s" "$jobname" "$script" "${FUNCNAME[-1]}" ;;
        4 ) printf "%-10s %-15s %-10s" "$jobname" "$script" "$call_function" ;;
        5 ) printf "%-10s %-15s %-10s %-15s" "$jobname" "$script" "$call_function" "$bt1" ;;
        * ) printf "%-10s %-15s %-10s %-30s" "$jobname" "$script" "$call_function" "$bt1 $bt2";;
    esac
}



#null fd for xtrace
exec 99>/dev/null


function verbose {
    #https://unix.stackexchange.com/questions/60041/bash-escape-individual-lines-from-x-echoing
    { BASH_XTRACEFD=99; }  2> /dev/null # silently disable xtrace
    tracestate="$(shopt -po xtrace)"
    set +x
    BASH_XTRACEFD=2
    
    local msg="$1"
    local level=${2:-1}
    [ -z "$level" ] && level=0
    local verbose_level=${verbose_level:-1}
    local verbosecolorstart=""
    local verbosecolorend=""
    
    [ "$level" -eq "$level" ] 2>/dev/null || echo "$(verbose_prefix $verbose_level 0) level $level must be a number" >&2
    
    msgsize=$(echo $msg | wc -c)
    if [ $msgsize -gt 200 ] ; then
        msg="${msg:0:100} ... ${msg: -100}"
    fi
    
    if (( $level <= $verbose_level )) ;  then
        local prefix=$(verbose_prefix $verbose_level $level)
        if [ -t 2 ] ; then
            verbosecolorstart=$purple
            (( "$level" == 0 )) && verbosecolorstart=$red
            verbosecolorend=$nocolor
        fi
        printf "$verbosecolorstart%s : %s$verbosecolorend\n" "$prefix" "$msg" >&2
        # >&98
    fi 
    
    eval "$tracestate"
}   

function get_jobid { 
	# from array_index, get full jobid (112223[idx] , or 12234)
	local idx=$1
	local ret=""
	for jobid in $(cat $QSUBARRAY_WS/jobids | xargs) ; do
		local range=$(cat $QSUBARRAY_WS/jobs/$jobid/range)
		local start=$(echo $range | awk '{ print $1}')
		local stop=$(echo $range | awk '{ print $2}')
		[ -z "$stop" ] && stop=$start
		if (( (idx >= start) && (idx <= stop) )); then
			if echo $jobid | grep -q "\[" ; then
				ret=$(echo $jobid | sed 's/\[\]/['$idx']/')
			else
				ret=$jobid
			fi
			break
		fi
	done
	[ -z "$ret" ] && this_is_a_bug "no jobid found for index $idx" && exit 1 
	verbose "job id for $idx : $ret" 4
	echo $ret
}

function caterror {
    for file in $* ; do
        if [ -s "$file" ]  ; then
            verbose "Error from $file:" 1 
            [ -t 2 ] && echo -ne "$red" >&2
            stdbuf -eL cat $file >&2
            [ -t 2 ] && echo -ne "$nocolor" >&2
        fi
    done
}

function catstdout {
    verbose "stdout from $@:" 3
    [ -t 1 ] && echo -ne "$green" 
    stdbuf -oL cat $@ 
    [ -t 1 ] && echo -ne "$nocolor" 
}

function catstderr {
    verbose "stderr from $@:" 3
    [ -t 2 ] && echo -ne "$orange" >&2
    stdbuf -eL cat $@ >&2
    [ -t 2 ] && echo -ne "$nocolor" >&2
}

function get_key {
    # return a uniq key  from stdin 
    md5sum | cut -d " " -f 1
}

function get_line {
    # return line number $1 from file $2
    head -n "$1" $2 | tail -1   
}


function this_is_a_bug { 
	verbose_level=4
	verbose "!!!! BUG in $(basename $0) !!!!" 0
	verbose "BUG: $1" 0
	verbose "job will continue to run, without monitoring" 0
	verbose "to remove the job : " 0
  	verbose "cat $QSUBARRAY_WS/jobids | xargs -n1 qdel" 0
}

function split_user_cmd {
    # split user cmd in a stdin feeder, a stdin consumer, and a worker
    # it's like:
    # cmd [opts] , where cmd take it's input from stdin
    # xargs [xargs_opts] cmd [opts], where xargs take it's input from stdin, given them as args to cmd
    # xargs -n5 cmd is the same as xargs -n5  | xargs -l1 cmd
    # xargs -l2 cmd is the same as xargs -l2  | xargs -l1 cmd
    # xargs -IINPUT cmd INPUT is the same as xargs -l1 | xargs -IINPUT cmd INPUT
    # so every xargs cmd can be splitted in two xargs commands, and the real user cmd: 
    #  - the first xargs is the stdin feeder (stdin_line_feeder)
    #  - the second xargs is the stdin consumer that always take one line of input ( stdin_consumer)
    #  - the last is the real user cmd (cmd_worker)
    # if no xargs cmd is given, stdin_line_feeder is 'cat', stdin_line_consumer is empty, and cmd_worker is "$@"
    #
    # so stdin_line_feeder can be used to split the input listing in lines that will be feeded to cmd_worker.
    local stdin_line_feeder_varname=$1
    local stdin_line_consumer_varname=$2
    local cmd_worker_varname=$3
    shift 3
    if echo "$*" | grep -q xargs ; then
        verbose "xargs mode" 3
        shift
        # use max 100k for the feeder, so the consumer won't run out of buffer
        eval $stdin_line_feeder_varname+='("xargs -s 100000")' ; eval $stdin_line_consumer_varname+='("xargs")'
        local GETOPT_OPT=$XARGS_GETOPT_ALL
        # find command name so we can add '--' just before to make getopt not process it: it's the first positional arg who is not processed by getopt (hacky)
        local TMP=$(getopt -q  $GETOPT_OPT -- "$@") 
        local CMD=$(echo ${TMP##* -- } | awk '{print $1}' | xargs)
        # make a new OPTS array from $@, adding -- before $CMD
        local OPTS=()
        for word in "$@" ; do
            [ "$word" == $CMD ] && OPTS+=("--") 
            OPTS+=("$word")
        done
        # now we can parse xargs opts. user cmd is after -- 
        local GETOPT=$(getopt -n $0 $GETOPT_OPT -- "${OPTS[@]}")
        eval set -- "$GETOPT"
        while true ; do
            case "$1" in 
                -I | -i | --replace  ) 
                    eval $stdin_line_feeder_varname+='("-l")' ; eval $stdin_line_consumer_varname+='("$1" "$2")' ; shift 2 
                ;;
                -l | -L | -n | -s | --max-chars ) 
                    eval $stdin_line_feeder_varname+='("$1$2")' ; eval $stdin_line_consumer_varname+='("-l")' ; shift 2 
                ;;
                -0 | -p | -r | -t | -x | --null | --interactive | --no-run-if-empty | --verbose | --show-limits | --exit | --version | --help | --NULL ) 
                    eval $stdin_line_feeder_varname+='("$1")' ; eval $stdin_line_consumer_varname+='("$1")' ; shift 
                ;;
                -a | -E | -e | -P | -d )
                    eval $stdin_line_feeder_varname+='("$1$2")' ; eval $stdin_line_consumer_varname+='("$1$2")' ; shift 2
                ;;
                --arg-file | --delimiter | --eof | --max-procs | --process-slot-var ) 
                    eval $stdin_line_feeder_varname+='("$1=$2")' ; eval $stdin_line_consumer_varname+='("$1=$2")' ; shift 2
                ;;
                --) shift ; break ;;
            esac
        done
    else
        eval $stdin_line_feeder_varname='(cat)'
        eval $stdin_line_consumer_varname='()'
    fi
    eval $cmd_worker_varname+='("$@")'
}


function check_getopt {
    # return the count +1 of accteped args of option $2, with getopt options variable name in $1 (see QSUBARRAY*_GETOPT_ALL after)
    local GETOPT_OPT_NAME="${1^^}_GETOPT_ALL"
    local GETOPT_OPT=${!GETOPT_OPT_NAME}
    [ -z "$GETOPT_OPT" ] && verbose  "can't get getopt options from $1 (\$$GETOPT_OPT_NAME empty)" 0 && exit 1
    
    local option="$2"
    # try with a dummy value
    GETOPT_TEST=$(getopt -q -n $0 $GETOPT_OPT -- "$option" dummy)
    [ $? -eq 1 ] && echo "0" && exit 1  # unknown opt
    eval set -- "$GETOPT_TEST"
    # check that th dummy value was accepted by opt 
    while [ $1 != "--" ] ; do shift ; done ; shift
    [ -z "$1" ] && echo "2" && exit  # dummy was accepted, so option take a value
    echo "1" && exit  
}

# define here qsubarray* options
# must follow this nomenclature:
# *_SHORT_OPTS : string for getopt -o
# *_LONG_OPTS : string for getopt --long
QSUBARRAY_WAIT_GETOPT_SHORT="d"
QSUBARRAY_WAIT_GETOPT_LONG="qstat,no-qstat,show-errors,no-show-errors,show-stdout,no-show-stdout,show-stderr,no-show-stderr,quiet,sort,hook:,workspace-link:,verbose::,loop-delay:"
QSUB_GETOPT_SHORT="hl:q:N:j:v:m:M:p:"
QSUB_GETOPT_LONG=""
QSUBARRAY_ADD_GETOPT_LONG="source:,reactivate,verbose:"
QSUBARRAY_ADD_GETOPT_SHORT="$QSUB_GETOPT_SHORT"
QSUBARRAY_GETOPT_LONG="stdin-flush-delay:,no-wait,subjobs:,max-lines:,max-jobs:,cache:,help"
QSUBARRAY_GETOPT_SHORT="d"

# we also need to be able to parse xargs opts
XARGS_GETOPT_SHORT="0a:E:e::i::I:l::L:n:prs:txP:d:"
XARGS_GETOPT_LONG="null,arg-file:,delimiter:,eof::,replace::,max-lines::,max-args:,interactive,no-run-if-empty,max-chars:,verbose,show-limits,exit,max-procs:,process-slot-var:,version,help,NULL"
XARGS_GETOPT_ALL="-o $XARGS_GETOPT_SHORT --long $XARGS_GETOPT_LONG"

# all variables names containing options, without suffix _*_OPTS
ALL_GETOPT_NAMES="QSUBARRAY QSUBARRAY_WAIT QSUB QSUBARRAY_ADD"

# options, with full getopt format
QSUBARRAY_WAIT_GETOPT_ALL="-o :$QSUBARRAY_WAIT_GETOPT_SHORT --long $QSUBARRAY_WAIT_GETOPT_LONG"
QSUBARRAY_ADD_GETOPT_ALL="-o :$QSUBARRAY_ADD_GETOPT_SHORT --long $QSUBARRAY_ADD_GETOPT_LONG"
QSUBARRAY_GETOPT_ALL="-o :$QSUBARRAY_GETOPT_SHORT$QSUBARRAY_ADD_GETOPT_SHORT$QSUBARRAY_WAIT_GETOPT_SHORT --long $QSUBARRAY_GETOPT_LONG,$QSUBARRAY_ADD_GETOPT_LONG,$QSUBARRAY_WAIT_GETOPT_LONG"




