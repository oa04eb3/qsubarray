% QSUBARRAY_BASHRC(5) Version 1.0 | qsub utils Documentation
[//]: # (pandoc -s -t man qsubarray_bashrc.md > qsubarray_bashrc.5)
NAME
====

**qsubarray_bashrc** — define and process defaults env vars on node side for use with **qsubarray(1)**

SYNOPSIS
========

| **qsubarray** \[ **--bashrc** ]

DESCRIPTION
===========

Default bashrc file to be sourced on node side by **qsubarray(1)**

This file reactivate the environnement on the submit host, depending of environnements variables submitted by **-v** option from **qsubarray(1)**


ENVIRONMENT
===========

**MODULESHOME**

: if defined, will source **$MODULESHOME/init/bash**

**LOADEDMODULES**

: if defined, will load specified modules

**CONDA_PREFIX**

: if defined, will load conda environnement

**VIRTUAL_ENV**

: if defined, will load virtual env


AUTHOR
======

Olivier Archer <Olivier.Archer@ifremer.fr>

SEE ALSO
========

**qsubarray(1)**
