% QSUBARRAY_AIT(1) Version 1.0 | qsub utils Documentation
[//]: # (pandoc -s -t man qsubarray_wait.md > qsubarray_wait.1)
NAME
====

**qsubarray_wait** — wait for a job submitted with **qsubarray(1)**

SYNOPSIS
========

| **qsubarray_wait** \[ **--[no-]qstat** ] \[ **--[no-]show-stdout** ] \[ **--[no-]show-stderr** ] \[ **--[no-]show-errors** ] \[ **--sort** ] qsubarray_workspace


DESCRIPTION
===========

Wait for a job submitted with **qsubarray(1)** to be finished. 

Options
-------

--qstat

:   display on stderr job queue status

--show-stdout

: cat on stdout the subjob's stdout, if the subjob has exited without errors (exit 0)

--show-stderr

: cat on stderr the subjob's stderr, if the subjob has exited without errors (exit 0)

--show-errors

: cat both stderr and stdout from the subjob to stderr, if the subjob has exited with errors (exit != 0)

--sort

: outputs are shown in the same order as stdin.

--hook="file or string" 

: bash script sourced before the monitoring starts. Might be a string.

--workspacefile=file

: store workspace directory to file. Shortcut for --hook='echo $QSUBARRAY_WS > file'.

AUTHOR
======

Olivier Archer <Olivier.Archer@ifremer.fr>

SEE ALSO
========

**qsubarray(1)**
