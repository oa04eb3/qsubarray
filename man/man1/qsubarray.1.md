% QSUBARRAY(1) Version 1.0 | qsub utils Documentation
[//]: # (pandoc -s -t man qsubarray.md > qsubarray.1)
NAME
====

**qsubarray** — submit job array using pipes 

SYNOPSIS
========

| **qsubarray** \[ qsub options ] \[ qsubarray_wait options ] \[ **-d** ] \[ **--max-subjobs=**_int_ | **--max-lines=**_int_ ] \[ **--stdin-flush-delay=**_int_ ]   _command_ \[ _command args_ ]  
| **qsubarray** \[ **--help** | **--version** ]

DESCRIPTION
===========

Submit job array using _PBS pro_ or _torque_ **qsub(1)** using pipes from stdin.


Options
-------

-d

:   debug messages. uses up to -dddd for maximum verbosity

--no-wait

: doesn't call **qsubarray_wait(1)** once the job is submited. Just echo the workspace to stdout. It is possible to call **qsubarray_wait(1)** later with the workrspace  as argument.

--max-lines=_int_
--max-jobs=_int_
--subjobs=_int_

:   number of subjobs to pack into one jobarray entry (default 1)

--source\[=_file_]

: bash file to be sourced on the node side before _command_ execution.
: this option may be specified more than once to allow multiples configurations to be sourced.

--reactivate

: reactivate environnement from the submit host, depending of passed environnment variables. (see **qsub(1)** _-v_ option to pass environement variables).

    * **MODULESHOME**  to  source **$MODULESHOME/init/bash**.

    * **LOADEDMODULES** to load specified modules.

    * **CONDA_PREFIX** to load conda environnement.

    * **VIRTUAL_ENV** to load virtual env.
    
: exemple to load the same conda environement from the submit host on the node side:
    **qsubarray -v CONDA_PREFIX --reactivate**

--stdin-flush-delay=_int_[smhd]
: if stdin is still open after _int_ seconds, will launch a job with already collected stdin. _int_ can be postfixed with s,m,h,d for seconds, minutes, hours, days.


qsub options
-------------

Many options from **qsub(1)** are available. (curently : -h -l -q -N -j -v -m -M -p )




Docker support
--------------

Docker support is enabled with the DOCKER_IMAGE environment variable.

  ```export DOCKER_IMAGE=docker.ifremer.fr/os/ubuntu:xenial```
  ```qsubarray -v CONDA_PREFIX```


EXAMPLES
========

Hello world
-----------

  	* simplest
  
```seq 1 10 | qsubarray xargs -n1 echo Hello world```

	* with mem and walltime
	
```seq 1 10 | qsubarray -l mem=200m,walltime=00:00:05 xargs -n1 echo Hello world```

    * reactivate conda

```module load anaconda-py2.7/4.3.13```

```source activate yourconda```

```seq 1 10 | qsubarray -v CONDA_PREFIX --reactivate xargs -n1 echo Hello world```

	(note that the *-v* option is a **qsub(1)** option to pass environment variables to jobs )

	* activate 'module' command
	

```seq 1 10 | qsubarray -v MODULESHOME --reactivate xargs -n1 echo Hello world```

  	* also reactivate loaded modules

```module load Gmt/5.4.2```

```seq 1 10 | qsubarray -v MODULESHOME,LOADEDMODULES, --reactivate xargs -n1 echo Hello world```

	* in docker (with mounts )
 
 ```export DOCKER_IMAGE=docker.ifremer.fr/os/ubuntu:xenial```
 
 ```seq 1 10 | qsubarray  -v DOCKER_IMAGE,MOUNT="/home/datawork-lops-siam;/home/datawork-cersat-public" xargs -n1 echo Hello world```
 
 

find files under a deep subtree
-------------------------------

### shell version

This command may take some hours:

```find /home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/ -name "*.nc" > ncfiles.lst```

Still using normal shell commands, this one is a little bit faster because of _xargs -P_

```ls -d /home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/*/* | xargs -IPATH -P2 find PATH -name "*.nc" > ncfiles.lst```

### qsubarray version

We just have to prefix _xargs_ with _qsubarray_:
```
ls -d /home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/*/* | qsubarray xargs -IPATH find PATH -name "*.nc" > ncfiles.lst
```


compute md5sum for each files under a deep subtree
--------------------------------------------------

_qsubarray_ commands can also be piped. Note that because off the high output from the left side pipe, it's not possible to have a qsub job with so many subjobs on the right side. So we limit the subjob count with _--max-subjobs=1000_. Each subjob will call several times _md5sum_ on different input file.


### shell version


```ls -d /home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/*/* | xargs -IPATH -P2 find PATH -name "*.nc" | xargs md5sum```


### qsubarray version

```
ls -d /home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/*/* | qsubarray xargs -IPATH find PATH -name "*.xml" | qsubarray --subjobs=1000 xargs md5sum
```

Note that the last _qsubarray_ invocation migth start before the preceding has ended: _qsubarray_, because it flush stdin periodicaly ( see --flush-stdin-delay )


Using _qsubarray_ as a server
---------------------------

It's also possible to run _qsubarray_ as a server.

for example:

```netcat -kl 4242 | qsubarray  xargs md5sum```

will set up a server listening on port 4242 that will preriodicaly submit a job to compute md5sum.
from anywhere from the network, it is possible to send filenames to the server with:

```ls /dataref/oc/modeles_ecmwf/analysis/2017/*.nc | netcat datarmor0 4242```



AUTHOR
======

Olivier Archer <Olivier.Archer@ifremer.fr>

SEE ALSO
========

**qsub(1)**, **xargs(1)**, **netcat(1)**, **qsubarray_wait(1)**
