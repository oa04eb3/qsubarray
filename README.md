# qsubarray

use jobarray and qsub as pipes and xargs

## datarmor use (no need install)

```
module use -a /home1/datahome/oarcher/appli/modulefiles/
module load qsubarray/val
man qsubarray
```

see [qsubarray(1)](man/man1/qsubarray.1.md). All [qsubarray_wait(1)](man/man1/qsubarray_wait.1.md) options can be passed to qsubarray.

 
## examples


```cat file.txt | mycommand```

```cat file.txt | qsubarray mycommand```

```cat file.txt | xargs -n1 mycommand```

```cat file.txt | qsubarray xargs -n1 mycommand```


