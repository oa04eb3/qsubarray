#!/bin/bash
script_dir=$(dirname $(readlink -f $0))

source $script_dir/etc/qsubarray.conf

install_dir="$1"

[ -z "$install_dir" ] && install_dir=$(readlink -f $QSUBARRAY_CONDA)
mkdir -p $install_dir
if [ ! -f $install_dir/bin/conda ] ; then
    wget -nc -c https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    chmod +x Miniconda2-latest-Linux-x86_64.sh
    ./Miniconda2-latest-Linux-x86_64.sh -u -b -p $install_dir
    rm Miniconda2-latest-Linux-x86_64.sh
fi

source $install_dir/bin/activate $install_dir
conda install -y pandoc 

