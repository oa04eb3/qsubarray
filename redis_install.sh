#!/bin/bash
install_dir=$(readlink -f lib/redis)
mkdir -p $install_dir
if [ ! -f $install_dir/bin/redis-server ] ; then
    wget http://download.redis.io/releases/redis-4.0.8.tar.gz
    tar xzf redis-4.0.8.tar.gz
    cd redis-4.0.8
    make PREFIX=$install_dir install
    cd ..
    rm redis-4.0.8.tar.gz 
    rm -r redis-4.0.8
fi
